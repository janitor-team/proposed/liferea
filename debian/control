Source: liferea
Section: web
Priority: optional
Maintainer: Paul Gevers <elbrus@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dpkg-dev (>= 1.16.1~),
               gla11y <!nocheck>,
               gobject-introspection | dh-sequence-gir,
               gsettings-desktop-schemas-dev,
               intltool,
               libgirepository1.0-dev,
               libglib2.0-dev,
               libgtk-3-dev,
               libjson-glib-dev,
               libpango1.0-dev,
               libpeas-dev,
               librsvg2-bin,
               libsoup2.4-dev,
               libsqlite3-dev,
               libwebkit2gtk-4.0-dev,
               libxml2-dev,
               libxslt1-dev,
               python3
Standards-Version: 4.5.0
Homepage: https://lzone.de/liferea/
Vcs-Browser: https://salsa.debian.org/debian/liferea
Vcs-Git: https://salsa.debian.org/debian/liferea.git
Rules-Requires-Root: no

Package: liferea
Architecture: any
Depends: default-dbus-session-bus | dbus-session-bus,
         gir1.2-peas-1.0,
         liferea-data (>= ${source:Upstream-Version}~),
         liferea-data (<< ${source:Upstream-Version}.0~),
         python3-cairo,
         python3-gi,
         python3-notify2,
         ${gir:Depends},
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Recommends: gir1.2-gstreamer-1.0,
            gir1.2-notify-0.7,
Suggests: kget,
          network-manager,
Description: feed/news/podcast client with plugin support
 Liferea is a feed reader, a news reader, and a podcast client that brings
 together all of the content from your favorite web subscriptions into a simple
 interface with an embedded graphical browser that's easy to organize and
 browse.
 It supports:
    * aggregating feeds in all the major syndication formats (including
      RSS/RDF, Atom, CDF, and more);
    * synchronizing feeds across devices, with TinyTinyRSS and
      TheOldReader support;
    * downloading articles for offline reading;
    * permanently saving headlines in news bins;
    * playing podcasts directly in Liferea's browser interface;
    * social networking / web integration so you can share your favorite news
      articles to Facebook, Google+, Reddit, Twitter, Slashdot, Digg, Yahoo and
      many more.
 .
 Liferea is an abbreviation for Linux Feed Reader.

Package: liferea-data
Architecture: all
Depends: ${misc:Depends}
Description: architecture independent data for liferea
 This package contains data files for liferea, a news aggregator for
 online news feeds.
 .
 This package does not depend on liferea, but it is unlikely to be
 of use by itself.
